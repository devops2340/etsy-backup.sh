#for now support only fedora and rhel family. (i have to add if for yum and apt-get or smth.)

BACKUP=/backup/backup_`hostname`_`date +"%Y%m%d"`
SYSTEM=${BACKUP}/system.txt
STORAGE=${BACKUP}/storage.txt
NETWORK=${BACKUP}/network.txt
PACKAGES=${BACKUP}/packages.txt

yum -y install nfs-utils tar gzip

mkdir -p /mnt/unix-backup
mkdir -p /backup
mkdir -pvm 770 ${BACKUP}

umount /mnt/unix-backup
timeout 10 mount -t nfs 192.168.1.6:/export/unix-backup /mnt/unix-backup
if [ $? -eq 0 ]; then
    echo "NFS export: '192.168.1.6:/unix-backup' mounted in '/mnt/unix-backup'" ; DESTINATION="192.168.1.6:/unix-backup" ;
else
    timeout 10 mount -o soft 192.168.1.7:/unix-backup /mnt/unix-backup
    if [ $? -eq 0 ]; then
        echo "NFS export: '192.168.1.7:/unix-backup' mounted in '/mnt/unix-backup'" ;  DESTINATION="192.168.1.7:/unix-backup" ;
    else
        echo "All NFS mounts failed."
    fi
fi


### system part
echo "=== date ========" >> ${SYSTEM} ; date >> ${SYSTEM} ;
echo "=== uptime ========" >> ${SYSTEM} ; uptime >> ${SYSTEM} ;
echo "=== uname -ar ========" >> ${SYSTEM} ; uname -ar >> ${SYSTEM} ;
echo "=== hostnamectl ========" >> ${SYSTEM} ; hostnamectl >> ${SYSTEM} ;
echo "=== sysctl -a ========" >> ${SYSTEM} ; sysctl -a >> ${SYSTEM} ;
echo "=== ps -ef ========" >> ${SYSTEM} ; ps -ef >> ${SYSTEM} ;
echo "=== cat /etc/selinux/config ========" >> ${SYSTEM} ; cat /etc/selinux/config >> ${SYSTEM} ;
echo "=== getsebool -a ========" >> ${SYSTEM} ; getsebool -a >> ${SYSTEM} ;

### storage part
echo "=== cat /etc/fstab ========" >> ${STORAGE} ; cat /etc/fstab >> ${STORAGE} ;
echo "=== pvs ========" >> ${STORAGE} ; pvs >> ${STORAGE} ;
echo "=== vgs ========" >> ${STORAGE} ; vgs >> ${STORAGE} ;
echo "=== lvs ========" >> ${STORAGE} ; lvs >> ${STORAGE} ;
echo "=== pvdisplay -m ========" >> ${STORAGE} ; pvdisplay -m >> ${STORAGE} ;
echo "=== vgdisplay ========" >> ${STORAGE} ; vgdisplay >> ${STORAGE} ;
echo "=== lvdisplay -m ========" >> ${STORAGE} ; lvdisplay -m >> ${STORAGE} ;
echo "=== lvs -a -o +devices ========" >> ${STORAGE} ; lvs -a -o +devices >> ${STORAGE} ;
echo "=== lsblk ========" >> ${STORAGE} ; lsblk >> ${STORAGE} ;
echo "=== df -hT ========" >> ${STORAGE} ; df -hT >> ${STORAGE} ;
echo "=== fdisk -l ========" >> ${STORAGE} ; fdisk -l >> ${STORAGE} ;
echo "=== multipath -ll ========" >> ${STORAGE} ; multipath -ll >> ${STORAGE} ;

### network part
echo "=== ip a ========" >> ${NETWORK} ; ip a >> ${NETWORK} ;
echo "=== ip r ========" >> ${NETWORK} ; ip r >> ${NETWORK} ;

### packages part
echo "=== yum repolist ========" >> ${PACKAGES} ; yum repolist >> ${PACKAGES} ;
echo "=== rpm -qa ========" >> ${PACKAGES} ; rpm -qa >> ${PACKAGES} ;

### others
cat /var/log/messages > ${BACKUP}/messages.txt ;
dmesg > ${BACKUP}/dmesg.txt ;
lshw > ${BACKUP}/lshw.txt ;
cp /root/anaconda-ks.cfg ${BACKUP}/kickstart.cfg ;
tar czf ${BACKUP}/etc_dir.tgz /etc/ ;
tar czf ${BACKUP}/boot_dir.tgz /boot/ ;
tar czf ${BACKUP}/varcrash_dir.tgz /var/crash ;
tar czf ${BACKUP}/varlog_dir.tgz /var/log ;

tar -zcvf ${BACKUP}.tar.gz ${BACKUP} ;
chmod 770 /backup ; chmod 770 ${BACKUP}.tar.gz ; chown lain:lain /backup ; chown lain:lain ${BACKUP}.tar.gz ;


if runuser -l lain -c 'BACKUP=/backup/backup_`hostname`_`date +"%Y%m%d"` ; mv ${BACKUP}.tar.gz /mnt/unix-backup/etc-archives/backup_`hostname`_`date +"%Y%m%d"`.tar.gz' ; then
    rm -rf ${BACKUP} ;
    echo "===================================================="
    echo "Backup completed and moved to the remote NFS export:" $DESTINATION"/backup_`hostname`_`date +"%Y%m%d"`.tar.gz" ;
else
    echo "===================================================="
    echo "Backup completed, but cannot be upload to the NFS export. Archive is stored localy in /backup/"
fi

#gzip /backup 
#mv /backup /mnt/unix-backup/  
#jeśli nfs się zamontował to wrzucić tam kopię, jeśli nie to zostawić kopię na /backup. i dać info na koniec skryptu co zostało zrobione z tego.
